package mysql;

import java.sql.*;
import java.util.Optional;

public class TestConnection {
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/test";
    private static final String USER = "user";
    private static final String PASS = "";

    private static String sqlQuery = "SELECT name, surname FROM acquaintances;";
//    private static String sqlQuery = "SELECT id, name FROM users WHERE name LIKE 'R%';";
    private static String sqlInsert = "INSERT INTO acquaintances (name, surname, sex, nin) " +
                                      "VALUES ('John', 'Python', 'M', '12345678900');";
    private static String sqlDelete = "DELETE FROM acquaintances WHERE name = 'John';";

    private Connection connection;
    private Statement statement;

    public static void main(String[] args) {
        TestConnection test = new TestConnection();
        Optional<Connection> connection = test.connectToDB();
        if (connection.isPresent()) {
            test.executeQuery(sqlQuery);
            test.executeInsert(sqlInsert);
            test.executeQuery(sqlQuery);
            test.executeDelete(sqlDelete);
            test.executeQuery(sqlQuery);
            test.disconnectFromDB();
        } else {
            System.out.println("Connection could not be established!");
        }
    }

    private void executeDelete(String sqlDelete) {
        try {
            statement.execute(sqlDelete);
            System.out.println("# Delete executed successfully.");
        } catch (SQLException exception) {
            System.out.println("Error deleting data!");
        }
    }

    private void executeQuery(String query) {
        try {
            System.out.println("-------------------------------");
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                System.out.println("# Query results:");
                System.out.print("> imię: " + resultSet.getString("name") + ", " +
                                 "nazwisko: " + resultSet.getString("surname") + '\n');

            }
            System.out.println("# Query executed successfully.");
            System.out.println("-------------------------------");
        } catch (SQLException exception) {
            System.out.println("Error retrieving data!");
        }

    }

    private void executeInsert(String insert) {
        try {
            statement.execute(insert);
            System.out.println("# Insert executed successfully.");
        } catch (SQLException exception) {
            System.out.println("Error inserting data!");
        }
    }

    public Optional<Connection> connectToDB() {
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(JDBC_URL, USER, PASS);
            statement = connection.createStatement();
            System.out.println("Successfully connected to DB.");
        } catch (ClassNotFoundException exception) {
            System.out.println("SQL driver error!");
        } catch (SQLException exception) {
            System.out.println("DB connection problem!");
        }

        return Optional.ofNullable(connection);
    }

    public void disconnectFromDB() {
        if (connection != null && statement != null) {
            try {
                statement.close();
                connection.close();
                System.out.println("Successfully disconnected from DB.");
            } catch (SQLException exception) {
                System.out.println("Cannot disconnect from DB!");
            }
        } else {
            System.out.println("No open connection!");
        }
    }
}
