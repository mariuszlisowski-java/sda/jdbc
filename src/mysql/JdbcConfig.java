package mysql;

import java.sql.*;
import java.util.Optional;

public class JdbcConfig {

    public static void main(String[] args) throws SQLException {
        String db = "world";
        String user = "user";
        String pass = "";
        String url = "jdbc:mysql://localhost:3306/" + db;
        Optional<Connection> connection = getConnection(url, user, pass);

        if (connection.isPresent()) {
            System.out.println("Successfully connected to database '" + db + "'.");

            // insert
            String insert = "INSERT INTO city (Name, CountryCode, District, Population) VALUES (?, ?, ?, ?);";
            PreparedStatement preparedStatement = connection.get().prepareStatement(insert);
            preparedStatement.setString(1, "Zakopane");
            preparedStatement.setString(2, "POL");
            preparedStatement.setString(3, "Malopolskie");
            preparedStatement.setInt(4, 198996);
            preparedStatement.execute();
            System.out.println("> entry inserted");

            // select
            String select = "SELECT id, Name, Population FROM city WHERE CountryCode = 'POL' AND Name = 'Zakopane';";
            Statement statement = connection.get().createStatement();
            ResultSet resultSet = statement.executeQuery(select);
            while (resultSet.next()) {
                System.out.println("id: " + resultSet.getInt("ID") +
                                   ", city: " + resultSet.getString("Name") +
                                   ", population: " + resultSet.getString("Population"));
            }

            // delete
            statement.execute("DELETE FROM city WHERE Name = 'Zakopane';");
            System.out.println("> entry deleted");

            connection.get().close();
            System.out.println("Successfully disconnected from database '" + db + "'.");
        }

    }

    public static Optional<Connection> getConnection(String url, String user, String pass) {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection =  DriverManager.getConnection(url, user, pass);

        } catch (ClassNotFoundException | SQLException exception) {
            System.err.println("Unalble to connect to db!");
        }

        return Optional.ofNullable(connection);
    }

}
