package mysql;

import java.sql.*;

public class ConnectionDemo {
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/world";
    private static final String USER = "user";
    private static final String PASS = "";

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        getRecords();
    }

    public static void getRecords() throws ClassNotFoundException, SQLException {
        Class.forName(JDBC_DRIVER);
        Connection connection;
        try {
            connection = DriverManager.getConnection(JDBC_URL, USER, PASS);
        } catch (SQLException exception) {
            System.out.println(exception.getMessage());
            return;
        }

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from city where district = 'virginia';");
        while (resultSet.next()) {
            System.out.println(resultSet.getString("ID") + " " +
                               resultSet.getString("Name") + " " +
                               resultSet.getString("CountryCode") + " " +
                               resultSet.getString("District") + " " +
                               resultSet.getInt("Population"));
        }

        connection.close();
    }
}
