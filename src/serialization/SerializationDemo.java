package serialization;

import java.io.*;

public class SerializationDemo {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Student studentA = new Student(23, "Kim", "kim@os.io");

        String filename = "student.text";

        // serialize
        OutputStream outputStream = new FileOutputStream(filename);
        ObjectOutputStream oos = new ObjectOutputStream(outputStream);
        oos.writeObject(studentA);
        oos.close();

        // deserialize
        InputStream inputStream = new FileInputStream(filename);
        ObjectInputStream ois = new ObjectInputStream(inputStream);
        Student studentB = (Student) ois.readObject();
        ois.close();

        System.out.println(studentA);
        System.out.println(studentB);
        System.out.println(studentB.equals(studentA));
    }

}
