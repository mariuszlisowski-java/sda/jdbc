package serialization;

import java.io.Serializable;
import java.util.Objects;

public class Student implements Serializable {
    private int age;
    private String name;
    private String email;

    public Student(int age, String name, String email) {
        this.age = age;
        this.name = name;
        this.email = email;
    }

    @Override
    public String  toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age && name.equals(student.name) && email.equals(student.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name, email);
    }
}
