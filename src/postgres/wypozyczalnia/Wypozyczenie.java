package postgres.wypozyczalnia;

import java.time.LocalDate;

public class Wypozyczenie {
    private long id;
    private Uzytkownik uzytkownik;
    private Produkt produkt;

    public Wypozyczenie(long id, Uzytkownik uzytkownik, Produkt produkt, LocalDate odDaty, LocalDate doDaty) {
        this.uzytkownik = uzytkownik;
        this.produkt = produkt;
        this.odDaty = odDaty;
        this.doDaty = doDaty;
    }

    private LocalDate odDaty;
    private LocalDate doDaty;

    public Uzytkownik getUzytkownik() {
        return uzytkownik;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public LocalDate getOdDaty() {
        return odDaty;
    }

    public LocalDate getDoDaty() {
        return doDaty;
    }

}
