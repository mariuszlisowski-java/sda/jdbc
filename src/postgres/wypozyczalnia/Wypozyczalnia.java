package postgres.wypozyczalnia;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Wypozyczalnia {
    long id;
    List<Wypozyczenie> wypozyczenia;

    public Wypozyczalnia() {
        this.wypozyczenia = new ArrayList<>();
    }

    public void wypozyczProdukt(long id, Uzytkownik uzytkownik, Produkt produkt, LocalDate odData, LocalDate doData) {
        wypozyczenia.add(new Wypozyczenie(id, uzytkownik, produkt, odData, doData));
        System.out.println(uzytkownik + " wypożyczył " + produkt);
    }

    public void oddajProdukt(Uzytkownik uzytkownik, Produkt produkt) {
        wypozyczenia.removeIf(wypozyczenie -> wypozyczenie.getProdukt().equals(produkt) &&
                wypozyczenie.getUzytkownik().equals(uzytkownik));
        System.out.println(uzytkownik + " oddał " + produkt);
    }

    void stworzBaze(String nazwa) {

    }



}
