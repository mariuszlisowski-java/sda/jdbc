package postgres.wypozyczalnia;

public class Gra extends Produkt {
    private long id;
    protected int maxLiczbaGraczy;

    public Gra(long id, String tytul, int maxLiczbaGraczy) {
        super(tytul);
        this.id = id;
        this.maxLiczbaGraczy = maxLiczbaGraczy;
    }
}
