package postgres.wypozyczalnia;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Wypozyczalnia wypozyczalnia = new Wypozyczalnia();
        Uzytkownik uzytkownik = new Uzytkownik(1, "Jan", "Kowalski", 33);
        Produkt film = new Film(1, "Hit",
                       new Aktor(1, "Kim", "Besinger"),
                       new Aktor(2,"Tom", "Cruise"));

        wypozyczalnia.wypozyczProdukt(1, uzytkownik, film, LocalDate.now(), LocalDate.now().plusDays(10));
        wypozyczalnia.oddajProdukt(uzytkownik, film);

    }

}
