package postgres.wypozyczalnia;

import java.util.Objects;

public abstract class Produkt {
    protected int dlugoscTrwania;
    protected String tytul;
    protected String krajPochodzenia;

    public Produkt(String tytul) {
        this.tytul = tytul;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produkt produkt = (Produkt) o;
        return dlugoscTrwania == produkt.dlugoscTrwania && Objects.equals(tytul, produkt.tytul) && Objects.equals(krajPochodzenia, produkt.krajPochodzenia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dlugoscTrwania, tytul, krajPochodzenia);
    }
}
