package postgres.wypozyczalnia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Film extends Produkt {
    private long id;
    protected List<Aktor> listaAktorow;

    public Film(long id, String tytul, Aktor ... actors) {
        super(tytul);
        this.id = id;
        listaAktorow = new ArrayList<>();
        listaAktorow.addAll(Arrays.asList(actors));
    }

    void dodajAktora(Aktor aktor) {
        listaAktorow.add(aktor);
    }

    @Override
    public String toString() {
        return "Film \"" + tytul + "\"";
    }
}
