package postgres.wypozyczalnia;

public class Uzytkownik {
    long id;
    private String imie;
    private String nazwisko;
    private int wiek;

    public Uzytkownik(long id, String imie, String nazwisko, int wiek) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Uzytkownik " + imie + " " + nazwisko;
    }
}
