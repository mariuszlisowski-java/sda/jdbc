package postgres.wypozyczalnia;

public class Aktor {
    private long id;
    private String imie;
    private String nazwisko;

    public Aktor(long id, String imie, String nazwisko) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }
}
