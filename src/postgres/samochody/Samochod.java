package postgres.samochody;

public class Samochod {
    private String marka;
    private String model;
    private int rocznik;
    private int iloscDrzwi;

    public Samochod(String marka, String model, int rocznik, int iloscDrzwi) {
        this.marka = marka;
        this.model = model;
        this.rocznik = rocznik;
        this.iloscDrzwi = iloscDrzwi;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getRocznik() {
        return rocznik;
    }

    public void setRocznik(int rocznik) {
        this.rocznik = rocznik;
    }

    public int getIloscDrzwi() {
        return iloscDrzwi;
    }

    public void setIloscDrzwi(int iloscDrzwi) {
        this.iloscDrzwi = iloscDrzwi;
    }
}
