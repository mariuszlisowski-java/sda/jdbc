package postgres.samochody;

import java.sql.*;

public class Main {
    static final String JDBC_DRIVER = "org.postgresql.Driver";
    static final String JDBC_URL = "jdbc:postgresql://localhost:5432/SDA64";
    static String USER = "postgres";
    static String PASSWORD = "postgres";

    public static void main(String[] args) {
        try {
            Class.forName(JDBC_DRIVER);
            Connection connection = DriverManager.getConnection(JDBC_URL, USER, PASSWORD);
            Statement statement = connection.createStatement();

            String create = "CREATE TABLE samochod (" +
                    "id SERIAL PRIMARY KEY," +
                    "marka varchar(255)," +
                    "model varchar(255)," +
                    "kolor varchar(255)," +
                    "rocznik int);";
//            statement.execute(create);
            
            String insert = "INSERT INTO samochod" +
                    "(marka, model, kolor, rocznik)" +
                    "VALUES" +
                    "('Audi', 'A3', 'czarny', 2018)," +
                    "('Audi', 'A1', 'biały', 2019)," +
                    "('Audi', 'A8', 'niebieski', 2011)," +
                    "('BMW', 'M235', 'zielony', 2010)," +
                    "('BMW', 'E45', 'czarny', 2013)," +
                    "('BMW', 'i8', 'czerwony', 2008)," +
                    "('Hyundai', 'i10', 'fioletowy', 2019)," +
                   "('Hyundai', 'i30', 'biały', 2020);";
//            statement.executeUpdate(insert);
            
            // 1. Wykonaj aktualizacje wszystkich marek z BMW na BMW2
            String updateBmw = "UPDATE samochod SET Marka = 'BMW' WHERE Marka = 'BMW2';";
            // String updateBmw = "UPDATE samochod SET Marka = 'BMW2' WHERE Marka = 'BMW';";
//            statement.executeUpdate(updateBmw);

            // 2. Do naszej tabeli dodaj kolumnę PoziomZniszczenia
            String addColumn = "ALTER TABLE samochod ADD PoziomZniszczenia varchar(255);";
//            statement.execute(addColumn);

            // 3. Do każdego samochodu w sposób losowy przypisz poziom zniszczenia od 0-10
            String damage = "UPDATE samochod SET poziomzniszczenia=floor(random() * (10 + 1));";
            statement.executeUpdate(damage);

            // 4. Usuń wszystkie rekordy marki Hyundai sprzed roku 2010
            String deleteHyunday = "DELETE FROM samochod WHERE Marka = 'Hyunday' AND rocznik < 2010";
//            statement.executeUpdate(deleteHyunday);

            // 5. Wprowadź 2 rekordy do tabeli
            String insertRecords = "INSERT INTO samochod" +
                                   "VALUES ('Mercedes', 'e330', 'srebrny', 2020);";

            // display table
            String query = "SELECT * FROM samochod";
            ResultSet rs = statement.executeQuery(query);
            while(rs.next()){
                String marka = rs.getString("marka");
                String model = rs.getString("model");
                String kolor = rs.getString("kolor");
                String rocznik = rs.getString("rocznik");
                System.out.println(marka + " " + model + " " + kolor + " " + rocznik);
            }
            rs.close();

            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
