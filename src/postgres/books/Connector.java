package postgres.books;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Connector {
    private static final String JDBC_DRIVER = "org.postgresql.Driver";
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/SDA64";
    private static String USER = "postgres";
    private static String PASSWORD = "postgres";

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Statement statement = createConnection(JDBC_DRIVER, JDBC_URL);

        Bookstore bookstore = new Bookstore();
        bookstore.dropTable(statement, "postgres/books");
        bookstore.createTable(statement, "postgres/books");
        bookstore.addRecord(statement, "Effective Java", "Joshua Bloch");
        bookstore.addRecord(statement, "Clean Code", "Robert Cecil Martin");
        bookstore.addRecord(statement, "Head First Java", "Kathy Sierra & Bert Bates ");
        bookstore.addISBNColumn(statement);
        bookstore.generateRandomISBNs(statement);

    }

    private static Statement createConnection(String driver, String url) throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        Connection connection = DriverManager.getConnection(url);

        return connection.createStatement();
    }

}
