package postgres.books;

import java.sql.SQLException;
import java.sql.Statement;

public class Bookstore {

    public void dropTable(Statement statement, String table) throws SQLException {
        statement.execute("DROP TABLE " + table);
    }

    public void createTable(Statement statement, String table) throws SQLException {
        statement.execute("CREATE TABLE " + table + " (" +
                "id SERIAL PRIMARY KEY," +
                "author varchar(255)," +
                "title varchar(255)" +
                ")" );
    }

    public void addRecord(Statement statement, String title, String author) throws SQLException {
        statement.executeUpdate("INSERT INTO postgres.books (author, title)" +
                                     "VALUES ('" + title + "', '" + author + "');" );
    }


    public void addISBNColumn(Statement statement) throws SQLException {
        statement.execute("ALTER TABLE postgres.books ADD isbn int");
    }

    public void generateRandomISBNs(Statement statement) throws SQLException {
        // 1000 - 9999
        statement.executeUpdate("UPDATE postgres.books SET isbn=floor(random() * (9999 - 1000 + 1) + 1000)");
    }

}
